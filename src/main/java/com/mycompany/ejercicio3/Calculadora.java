/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejercicio3;

/**
 *
 * @author xtian
 */
public class Calculadora {

    /**
     * @return the sumar
     */
    public int getCalculo() {
        return sumar;
    }

    /**
     * @param sumar the sumar to set
     */
    public void setCalculo(int calculo) {
        this.sumar = sumar;
    }

    /**
     * @return the n1
     */
    public int getCapital() {
        return capital;
    }

    /**
     * @param n1 the n1 to set
     */
    public void setCapital(int capital) {
        this.capital = capital;
    }

    /**
     * @return the n2
     */
   // public int getN2() {
   //     return n2;
   // }

    /**
     * @param n2 the n2 to set
     */
   // public void setN2(int n2) {
   //     this.n2 = n2;
   //}
    
    /**
     * @return the n2
     */
    public float getTasaAnual() {
        return tasaAnual;
    }

    /**
     * @param n2 the n2 to set
     */
    public void setTasaAnual(float tasaAnual) {
        this.tasaAnual = tasaAnual;
    }

    /**
     * @return the n3
     */
    public int getNumeroAnos() {
        return numeroAnos;
    }
 
    /**
     * @param n3 the n3 to set
     */
    public void setNumeroAnos(int numeroAnos) {
        this.numeroAnos = numeroAnos;
    }
    
    private int capital;
    //private int n2;
    private float tasaAnual;
    private int numeroAnos;
    private int sumar;
    
  //  public int sumar(){
            
  //   return (int) (this.getN1()*this.getN2()*this.getN3());
  //  }
    
     public int calculo(){
            
     return (int) (this.getCapital()*(this.getTasaAnual()/100)*this.getNumeroAnos());
    }

   public int calculo(int capital, float tasaAnual, int numeroAnos) {
       this.setCapital(capital);
       this.setTasaAnual(tasaAnual);
       this.setNumeroAnos(numeroAnos);
       return calculo();
  }
   
    
}
